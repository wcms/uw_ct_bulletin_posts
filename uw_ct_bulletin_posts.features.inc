<?php

/**
 * @file
 * uw_ct_bulletin_posts.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_bulletin_posts_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_bulletin_posts_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_bulletin_posts_node_info() {
  $items = array(
    'uw_ct_bulletins' => array(
      'name' => t('Bulletin post'),
      'base' => 'node_content',
      'description' => t('Bulletins that appear on the home page in the "bulletins" box and on the "all bulletins" page. Not to be confused with the Daily Bulletin, which this is not.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Keep bulletins short and to the point. You can provide related links to additional information.'),
    ),
  );
  return $items;
}

/**
 * Implements hook_rdf_default_mappings().
 */
function uw_ct_bulletin_posts_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: comment_node_uw_ct_bulletins.
  $schemaorg['comment']['comment_node_uw_ct_bulletins'] = array(
    'rdftype' => array(
      0 => 'sioc:Post',
      1 => 'sioct:Comment',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'comment_body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'pid' => array(
      'predicates' => array(
        0 => 'sioc:reply_of',
      ),
      'type' => 'rel',
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
  );

  // Exported RDF mapping: uw_ct_bulletins.
  $schemaorg['node']['uw_ct_bulletins'] = array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'field_date_time' => array(
      'predicates' => array(),
    ),
    'field_bulletin_type' => array(
      'predicates' => array(),
    ),
    'field_short_summary' => array(
      'predicates' => array(),
    ),
    'field_category' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_updates' => array(
      'predicates' => array(),
    ),
  );

  // Exported RDF mapping: uw_vocab_bulletin_categories.
  $schemaorg['taxonomy_term']['uw_vocab_bulletin_categories'] = array(
    'rdftype' => array(
      0 => 'skos:Concept',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'rdfs:label',
        1 => 'skos:prefLabel',
      ),
    ),
    'description' => array(
      'predicates' => array(
        0 => 'skos:definition',
      ),
    ),
    'vid' => array(
      'predicates' => array(
        0 => 'skos:inScheme',
      ),
      'type' => 'rel',
    ),
    'parent' => array(
      'predicates' => array(
        0 => 'skos:broader',
      ),
      'type' => 'rel',
    ),
  );

  return $schemaorg;
}
