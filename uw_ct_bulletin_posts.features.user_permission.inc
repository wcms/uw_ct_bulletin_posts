<?php

/**
 * @file
 * uw_ct_bulletin_posts.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_bulletin_posts_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_ct_bulletins content'.
  $permissions['create uw_ct_bulletins content'] = array(
    'name' => 'create uw_ct_bulletins content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_ct_bulletins content'.
  $permissions['delete any uw_ct_bulletins content'] = array(
    'name' => 'delete any uw_ct_bulletins content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_bulletins content'.
  $permissions['delete own uw_ct_bulletins content'] = array(
    'name' => 'delete own uw_ct_bulletins content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in uw_vocab_bulletin_categories'.
  $permissions['delete terms in uw_vocab_bulletin_categories'] = array(
    'name' => 'delete terms in uw_vocab_bulletin_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_ct_bulletins content'.
  $permissions['edit any uw_ct_bulletins content'] = array(
    'name' => 'edit any uw_ct_bulletins content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_bulletins content'.
  $permissions['edit own uw_ct_bulletins content'] = array(
    'name' => 'edit own uw_ct_bulletins content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in uw_vocab_bulletin_categories'.
  $permissions['edit terms in uw_vocab_bulletin_categories'] = array(
    'name' => 'edit terms in uw_vocab_bulletin_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter uw_ct_bulletins revision log entry'.
  $permissions['enter uw_ct_bulletins revision log entry'] = array(
    'name' => 'enter uw_ct_bulletins revision log entry',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_bulletins authored by option'.
  $permissions['override uw_ct_bulletins authored by option'] = array(
    'name' => 'override uw_ct_bulletins authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_bulletins authored on option'.
  $permissions['override uw_ct_bulletins authored on option'] = array(
    'name' => 'override uw_ct_bulletins authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_bulletins promote to front page option'.
  $permissions['override uw_ct_bulletins promote to front page option'] = array(
    'name' => 'override uw_ct_bulletins promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_bulletins published option'.
  $permissions['override uw_ct_bulletins published option'] = array(
    'name' => 'override uw_ct_bulletins published option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_bulletins revision option'.
  $permissions['override uw_ct_bulletins revision option'] = array(
    'name' => 'override uw_ct_bulletins revision option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_bulletins sticky option'.
  $permissions['override uw_ct_bulletins sticky option'] = array(
    'name' => 'override uw_ct_bulletins sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_ct_bulletins content'.
  $permissions['search uw_ct_bulletins content'] = array(
    'name' => 'search uw_ct_bulletins content',
    'roles' => array(),
    'module' => 'search_config',
  );

  return $permissions;
}
