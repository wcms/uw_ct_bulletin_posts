<?php

/**
 * @file
 * uw_ct_bulletin_posts.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_ct_bulletin_posts_taxonomy_default_vocabularies() {
  return array(
    'uw_vocab_bulletin_categories' => array(
      'name' => 'Bulletin categories',
      'machine_name' => 'uw_vocab_bulletin_categories',
      'description' => 'User-editable list of bulletin categories.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
