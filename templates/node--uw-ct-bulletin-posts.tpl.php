<?php

/**
 * @file
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="node-inner">

    <div class="bulletins_head">
    <?php
    $uw_ct_bulletin_posts_type = taxonomy_term_load($content['field_vocab_bulletin_type']['#items'][0]['tid']);
    ?>
    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><img src="<?php echo base_path() . drupal_get_path('module', 'uw_ct_bulletin_posts'); ?>/images/<?php print check_plain($uw_ct_bulletin_posts_type->field_machine_name[LANGUAGE_NONE][0]['value']); ?>_big.png" width="48" height="48" alt="<?php print check_plain($uw_ct_bulletin_posts_type->name); ?>:" /><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php else: ?>
      <h1<?php print $title_attributes; ?>><img src="<?php echo base_path() . drupal_get_path('module', 'uw_ct_bulletin_posts'); ?>/images/<?php print check_plain($uw_ct_bulletin_posts_type->field_machine_name[LANGUAGE_NONE][0]['value']); ?>_big.png" width="48" height="48" alt="<?php print check_plain($uw_ct_bulletin_posts_type->name); ?>:" /><?php print $title; ?></h1>
    <?php endif; ?>
      <div><?php print render($content['field_date_time']); ?></div>
      <div><?php print render($content['field_location']); ?></div>
    </div>

    <?php print $user_picture; ?>

    <div class="content_node"<?php print $content_attributes; ?>>
      <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        hide($content['field_category']);
        hide($content['field_vocab_bulletin_type']);
        print render($content);
       ?>
    </div>

    <?php if (!empty($content['links']['terms'])): ?>
      <div class="terms"><?php print render($content['links']['terms']); ?></div>
    <?php endif;?>

    <?php if (!empty($content['links'])): ?>
      <div class="links"><?php print render($content['links']); ?></div>
    <?php endif; ?>
    <div class="postinfo">

      <?php if (!empty($content['field_category'])): ?>
        <div class="bulletins_categories">Filed under: <?php print render($content['field_category']); ?></div>
      <?php endif; ?>      
      <?php if ($display_submitted): ?>
        <div class="submitted">Filed by: <?php print $name; ?>  on <span property="dc:date dc:created" content="<?php print format_date($node->created, 'custom', "c"); ?>" datatype="xsd:dateTime"><?php print format_date($node->created, 'custom', 'F j, Y'); ?><span></div>
      <?php endif; ?>
    
    </div>


  </div> <!-- /node-inner -->
</div> <!-- /node-->

<?php print render($content['comments']); ?>
