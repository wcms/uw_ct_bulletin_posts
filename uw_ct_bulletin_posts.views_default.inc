<?php

/**
 * @file
 * uw_ct_bulletin_posts.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_bulletin_posts_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'uw_bulletins';
  $view->description = 'Listing pages for the Bulletin post content type.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Bulletins';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Bulletins';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '7';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'full';
  $handler->display->display_options['row_options']['links'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Sticky */
  $handler->display->display_options['sorts']['sticky']['id'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['table'] = 'node';
  $handler->display->display_options['sorts']['sticky']['field'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['order'] = 'DESC';
  /* Sort criterion: Content: Date/time -  start date (field_date_time) */
  $handler->display->display_options['sorts']['field_date_time_value']['id'] = 'field_date_time_value';
  $handler->display->display_options['sorts']['field_date_time_value']['table'] = 'field_data_field_date_time';
  $handler->display->display_options['sorts']['field_date_time_value']['field'] = 'field_date_time_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'uw_ct_bulletins' => 'uw_ct_bulletins',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<p>No bulletins found.</p>';
  $handler->display->display_options['empty']['area']['format'] = 'uw_tf_standard';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'uw_ct_bulletins' => 'uw_ct_bulletins',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Global: Composite Filter */
  $handler->display->display_options['filters']['composite_views_filter']['id'] = 'composite_views_filter';
  $handler->display->display_options['filters']['composite_views_filter']['table'] = 'views';
  $handler->display->display_options['filters']['composite_views_filter']['field'] = 'composite_views_filter';
  $handler->display->display_options['filters']['composite_views_filter']['group'] = 1;
  $handler->display->display_options['filters']['composite_views_filter']['exposed'] = TRUE;
  $handler->display->display_options['filters']['composite_views_filter']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['composite_views_filter']['expose']['label'] = 'Dates';
  $handler->display->display_options['filters']['composite_views_filter']['expose']['operator'] = 'composite_views_filter_op';
  $handler->display->display_options['filters']['composite_views_filter']['expose']['identifier'] = 'dates';
  $handler->display->display_options['filters']['composite_views_filter']['expose']['required'] = TRUE;
  $handler->display->display_options['filters']['composite_views_filter']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    16 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    17 => 0,
  );
  $handler->display->display_options['filters']['composite_views_filter']['composite_views_filter']['groups'] = 'All|- Any -
Upcoming|Current & upcoming
Past|Past';
  $handler->display->display_options['filters']['composite_views_filter']['composite_views_filter']['default_group'] = 'Upcoming';
  $handler->display->display_options['filters']['composite_views_filter']['composite_views_filter']['classification'] = array(
    'field_date_time_value' => 'Upcoming',
    'field_date_time_value_1' => 'Past',
  );
  /* Filter criterion: Content: Date/time -  start date (field_date_time) */
  $handler->display->display_options['filters']['field_date_time_value']['id'] = 'field_date_time_value';
  $handler->display->display_options['filters']['field_date_time_value']['table'] = 'field_data_field_date_time';
  $handler->display->display_options['filters']['field_date_time_value']['field'] = 'field_date_time_value';
  $handler->display->display_options['filters']['field_date_time_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_date_time_value']['group'] = 1;
  $handler->display->display_options['filters']['field_date_time_value']['expose']['operator_id'] = 'field_date_time_value_op';
  $handler->display->display_options['filters']['field_date_time_value']['expose']['label'] = 'Date/time -  start date (field_date_time)';
  $handler->display->display_options['filters']['field_date_time_value']['expose']['operator'] = 'field_date_time_value_op';
  $handler->display->display_options['filters']['field_date_time_value']['expose']['identifier'] = 'field_date_time_value';
  $handler->display->display_options['filters']['field_date_time_value']['default_date'] = '12AM today';
  $handler->display->display_options['filters']['field_date_time_value']['year_range'] = '-0:+3';
  /* Filter criterion: Content: Date/time -  start date (field_date_time) */
  $handler->display->display_options['filters']['field_date_time_value_1']['id'] = 'field_date_time_value_1';
  $handler->display->display_options['filters']['field_date_time_value_1']['table'] = 'field_data_field_date_time';
  $handler->display->display_options['filters']['field_date_time_value_1']['field'] = 'field_date_time_value';
  $handler->display->display_options['filters']['field_date_time_value_1']['operator'] = '<';
  $handler->display->display_options['filters']['field_date_time_value_1']['group'] = 1;
  $handler->display->display_options['filters']['field_date_time_value_1']['default_date'] = '12AM today';
  $handler->display->display_options['filters']['field_date_time_value_1']['year_range'] = '-10:+0';
  /* Filter criterion: Content: Bulletin type (field_bulletin_type) */
  $handler->display->display_options['filters']['field_bulletin_type_value']['id'] = 'field_bulletin_type_value';
  $handler->display->display_options['filters']['field_bulletin_type_value']['table'] = 'field_data_field_bulletin_type';
  $handler->display->display_options['filters']['field_bulletin_type_value']['field'] = 'field_bulletin_type_value';
  $handler->display->display_options['filters']['field_bulletin_type_value']['group'] = 1;
  $handler->display->display_options['filters']['field_bulletin_type_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_bulletin_type_value']['expose']['operator_id'] = 'field_bulletin_type_value_op';
  $handler->display->display_options['filters']['field_bulletin_type_value']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['field_bulletin_type_value']['expose']['operator'] = 'field_bulletin_type_value_op';
  $handler->display->display_options['filters']['field_bulletin_type_value']['expose']['identifier'] = 'type';
  $handler->display->display_options['filters']['field_bulletin_type_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    16 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    17 => 0,
  );
  /* Filter criterion: Content: Category (field_category) */
  $handler->display->display_options['filters']['field_category_tid']['id'] = 'field_category_tid';
  $handler->display->display_options['filters']['field_category_tid']['table'] = 'field_data_field_category';
  $handler->display->display_options['filters']['field_category_tid']['field'] = 'field_category_tid';
  $handler->display->display_options['filters']['field_category_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_category_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_category_tid']['expose']['operator_id'] = 'field_category_tid_op';
  $handler->display->display_options['filters']['field_category_tid']['expose']['label'] = 'Category';
  $handler->display->display_options['filters']['field_category_tid']['expose']['operator'] = 'field_category_tid_op';
  $handler->display->display_options['filters']['field_category_tid']['expose']['identifier'] = 'category';
  $handler->display->display_options['filters']['field_category_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    16 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    17 => 0,
  );
  $handler->display->display_options['filters']['field_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_category_tid']['vocabulary'] = 'uw_vocab_bulletin_categories';
  $handler->display->display_options['path'] = 'bulletins';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Bulletins';
  $handler->display->display_options['menu']['name'] = 'main-menu';

  /* Display: Feed */
  $handler = $view->new_display('feed', 'Feed', 'feed');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['style_plugin'] = 'rss';
  $handler->display->display_options['row_plugin'] = 'node_rss';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'uw_ct_bulletins' => 'uw_ct_bulletins',
  );
  /* Filter criterion: Content: Date/time -  start date (field_date_time) */
  $handler->display->display_options['filters']['field_date_time_value']['id'] = 'field_date_time_value';
  $handler->display->display_options['filters']['field_date_time_value']['table'] = 'field_data_field_date_time';
  $handler->display->display_options['filters']['field_date_time_value']['field'] = 'field_date_time_value';
  $handler->display->display_options['filters']['field_date_time_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_date_time_value']['default_date'] = '12AM today';
  $handler->display->display_options['filters']['field_date_time_value']['year_range'] = '-0:+3';
  $handler->display->display_options['path'] = 'bulletins.xml';
  $handler->display->display_options['displays'] = array(
    'default' => 'default',
    'page' => 'page',
  );
  $export['uw_bulletins'] = $view;

  return $export;
}
